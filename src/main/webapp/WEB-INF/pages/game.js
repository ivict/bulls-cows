
function displayImage(imgId, imageName) {
	img = document.getElementById(imgId);
	if ( img == null ) {
		return;
	}
	img.src = pageContextPath + "/images/" + imageName + ".png";
}

function formatUTCTime(dateTime) {
	timeStr = '';
	hours = dateTime.getUTCHours();
	if ( hours < 10 ) {
		timeStr += '0';
	}
	timeStr += hours + ':';
	minutes = dateTime.getUTCMinutes();
	if ( minutes < 10 ) {
		timeStr += '0';
	}
	timeStr += minutes + ':';
	seconds = dateTime.getUTCSeconds();
	if ( seconds < 10 ) {
		timeStr += '0';
	}
	timeStr += seconds;
	return timeStr;
}

function formatTime(dateTime) {
	timeStr = '';
	hours = dateTime.getHours();
	if ( hours < 10 ) {
		timeStr += '0';
	}
	timeStr += hours + ':';
	minutes = dateTime.getMinutes();
	if ( minutes < 10 ) {
		timeStr += '0';
	}
	timeStr += minutes + ':';
	seconds = dateTime.getSeconds();
	if ( seconds < 10 ) {
		timeStr += '0';
	}
	timeStr += seconds;
	return timeStr;
}

function displayTimerInfo() {
	// Later can be use for timer, now for debug purposes
	startTime = new Date(userData.startTime);
	timeStartText = document.getElementById('time-start-text');
	timeStartText.innerHTML = messages.TIME_VALUE.format(formatTime(startTime));
	gameDurationText = document.getElementById('game-duration-text');
	diff = new Date(new Date().getTime() - userData.startTime);
	gameDurationText.innerHTML = messages.GAME_DURATION_VALUE.format(formatUTCTime(diff));
}

function makeFocusable(divId) {
	div = document.getElementById(divId);
	const initialClassName = div.className; 
	//Draw focus
	div.onmouseover = function() {
		clearFocus(ifocus, focusable);
		ifocus = findex[this.id];
		setFocus(ifocus, focusable);
	};
	//Clear focus
	div.onmouseout = function() {
		ifocus = findex[this.id];
		clearFocus(ifocus, focusable);
	};
}

function parseResponse(requestResult) {//Parse UserData bean
	stepsCount = requestResult.stepsCount;
	//No user steps, reset to initial state.
	if ( stepsCount == 0 ) {
		return;
	}
	bullsCount = requestResult.bullsCount;
	cowsCount = requestResult.cowsCount;
	displayImage('bulls-count-img', bullsCount + 'b');
	displayImage('cows-count-img', cowsCount + 'c');
	
	number1 = requestResult.number1;
	number2 = requestResult.number2;
	number3 = requestResult.number3;
	number4 = requestResult.number4;
	// Write step into history
	historyText = document.getElementById('history-text');
	historyText.innerHTML += messages.HISTORY_ENTRY.format(number1, number2, number3, number4, bullsCount, cowsCount);
	history = document.getElementById('history');
	history.scrollTop = history.scrollHeight;//TODO: tune history text control

	userData = requestResult;
	displayTimerInfo();
}

function redirectToRating() {
	funcParam = document.getElementById('rate-func');
	funcParam.name = requests.GET_RATE;
	funcParam.value = 0;
	rateRequest = document.getElementById('rate-request');
	rateRequest.action = requests.RATE;
	rateRequest.submit();
}


/**
 * Global variables.
 * Make after load page.
 */
var ifocus;
var focusable;
var findex = {};
var fclassName = {};

//TODO: continue refactoring after complete implementation
var GVCtrlIndex = 0;
var FFocusedCtrl = function() {
	this.index = GVCtrlIndex++;
}
var FNumberCtrl = function ( index ) {
	this.prototype = new FFocusedCtrl(); 
	this.index = numberIndex;
	this.isHide = true;
	this.value = 0;
}

var numbers = { 'values' : { 'number1': 0, 'number2' : 0, 'number3' : 0, 'number4' : 0},
                'isHide' : { 'number1': true, 'number2' : true, 'number3' : true, 'number4' : true}};
/**
 * FNextStepCtrl implementation
 */
var FNextStepCtrl = function ( states, currentState ) {
	this.prototype = new FFocusedCtrl(); 
	this.states = states;
	this.current = currentState;
};
FNextStepCtrl.prototype.isInFocus = function () {
	return findex['next-step-button'] == ifocus;
}
FNextStepCtrl.prototype.switchToState = function (state) {
	stateCode = this.states[state];
	if ( stateCode == null ) {
		return;
	}
	this.current = state;
	displayImage('next-step-button-img', 'next-step-button-' + this.current);
}
FNextStepCtrl.prototype.resetState = function() {
	if ( nextStepCtrl.isInFocus() ) {
		nextStepCtrl.switchToState('sel');
	} else {
		nextStepCtrl.switchToState('up');
	}
}
FNextStepCtrl.prototype.response = function () {
	xmlhttp = this;//Callback for HTTP request
	if ( !xmlhttp ) {
	    //TODO request from iframe
	    return;
	}
	if ( xmlhttp.readyState == 4 ) {
	    if ( xmlhttp.status != 200 ) {
	        return;
	    }
	    requestResult = JSON.parse(xmlhttp.responseText);
	    parseResponse(requestResult);
	}
}
FNextStepCtrl.prototype.request = function () {
	if ( this.htmlRequest ) {//Previous request incomplete
	    return;
	}
	if ( userData.gameOver ) {
		redirectToRating();
		return;
	}
	this.htmlRequest = createXMLHttpRequest();
	xmlhttp = this.htmlRequest;
	if ( !xmlhttp ) {
		document.title = messages.BROWSER_NOT_SUPPORTED;
	    //TODO request from iframe
	    return;
	}
	if ( !this.response ) {//INVALID STATE CALLBACK IS EMPTY
		return;
	}
	xmlhttp.open('POST', requests.NEXT_STEP, false);
	xmlhttp.onreadystatechange = this.response;
	xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	params = requests.NUMBER1 + '=' + numbers.values[requests.NUMBER1] + '&'
		+ requests.NUMBER2 + '=' + numbers.values[requests.NUMBER2] + '&'
		+ requests.NUMBER3 + '=' + numbers.values[requests.NUMBER3] + '&'
		+ requests.NUMBER4 + '=' + numbers.values[requests.NUMBER4];
	xmlhttp.send(params);
	this.htmlRequest = null;//Make request complete
}
var nextStepCtrl = new FNextStepCtrl({'up' : 0, 'sel' : 1, 'inv': 2, 'down': 3}, 'up');

function nextNumber(indexInFocusCycle) {
	divId = focusable[indexInFocusCycle];
	var number = numbers.values[divId];
	//Not a number control
	if ( number == null ) {
		return;
	}
	number += ( numbers.isHide[divId] ? 0 : 1);
	if ( number > 9 ) {
		number = 0;
	}
	setInputNumber(divId, number);
	checkDuplicatesAndMaskIfFound(divId);
}

function prevNumber(indexInFocusCycle) {
	divId = focusable[indexInFocusCycle];
	var number = numbers.values[divId];
	//Not a number control
	if ( number == null ) {
		return;
	}
	number -= ( numbers.isHide[divId] ? 0 : 1);
	if ( number < 0 ) {
		number = 9;
	}
	setInputNumber(divId, number);
	checkDuplicatesAndMaskIfFound(divId);
}

function setInputNumber(numberCtrlId, number) {
	imgId = numberCtrlId + '-img';
	displayImage(imgId, number);
	numbers.isHide[numberCtrlId] = false;
	numbers.values[numberCtrlId] = number;
}

function isUserInputInvalid() {
	//Check same numbers (wrong user input) and check range
	for ( var key in numbers.values ) {
		if ( numbers.values[key] == null ) {
			return true;
		} 
		if ( numbers.values[key] < 0 ) {
			return true;
		} 
		if ( numbers.values[key] > 9 ) {
			return true;
		} 
		if ( hasDuplicate(key, numbers.values) ) {
			return true;
		};
	};
	return false;
} 

function checkDuplicatesAndMaskIfFound(numberCtrlId) {
	//Check same numbers (wrong user input)
	for ( var key in numbers.values ) {
		if ( key == numberCtrlId ) {
			continue;
		} 
		if ( hasDuplicate(key, numbers.values) ) {
			displayImage(key + '-img', 'asterisk');
			numbers.isHide[key] = true;
			continue;
		}
		displayImage(key + '-img', numbers.values[key]);
		numbers.isHide[key] = false;
	};
}

/**
 * Check associative array on duplicate values. 
 * @param key
 * @param map associative array
 */
function hasDuplicate(key, map) {
	value = map[key];
	if ( value == null ) {
		return false;
	}
	for ( var k in map ) {
		if ( k == key ) {
			continue;
		}
		if ( map[k] == value ) {
			return true;
		};
	}
	return false;
}

function setTabIndex(elementId, indexValue) {
	element = document.getElementById(elementId);
	if ( !element ) {
		return;
	}
	element.tabindex = indexValue;
} 

//Single call after page load
function makeFocusCycle(arrayOfIds) {
	for ( var i = 0; i < arrayOfIds.length; i++ ) {
		divId = arrayOfIds[i];
		makeFocusable(divId);
		div = document.getElementById(divId);
		div.onclick = function() {
			clearFocus(ifocus, focusable);
			ifocus = findex[this.id];
			setFocus(ifocus, focusable);
			nextNumber(ifocus);
			pushToNextStepButton();
		};
		findex[divId] = i;
		fclassName[divId] = div.className;
	};
	//Update global variable
	focusable = arrayOfIds;
	ifocus = 0;
}

function clearFocus(i, elementsIds) {
	if ( i < 0 ) {
		return;
	}
	if ( i >= elementsIds.length ) {
		return;
	}
	elementId = elementsIds[i];
	element = document.getElementById(elementId);
	element.className = fclassName[elementId];
	//TODO: refactoring try unweave code
	if ( nextStepCtrl.isInFocus() ) {
		nextStepCtrl.switchToState('up');
	}
}

function setFocus(i, elementsIds) {
	if ( i < 0 ) {
		return;
	}
	if ( i >= elementsIds.length ) {
		return;
	}
	elementId = elementsIds[i];
	element = document.getElementById(elementId);
	element.className = fclassName[elementId] + ' focused';
	//TODO: refactoring try unweave code
	if ( nextStepCtrl.isInFocus() ) {
		nextStepCtrl.switchToState('sel');
	}
	element.focus();
		
}

function moveFocusToPrev() {
	if ( ifocus == null ) {
		return;
	}
	clearFocus(ifocus, focusable);
	if ( ifocus <= 0 ) {
		ifocus = focusable.length - 1;
	} else {
		ifocus--;
	}
	setFocus(ifocus, focusable);
}

function moveFocusToNext() {
	if ( ifocus == null ) {
		return;
	}
	clearFocus(ifocus, focusable);
	if ( ifocus >= focusable.length ) {
		ifocus = 0;
	} else {
		ifocus++;
	}
	setFocus(ifocus, focusable);
}

function pushToNextStepButton() {
	if ( !nextStepCtrl.isInFocus() ) {
		return;
	}
	
	gameOver = userData.isGameOver;
	if ( isUserInputInvalid() || gameOver) {//Reset invalid state if user input is valid
		nextStepCtrl.switchToState('inv');
	} else {
		nextStepCtrl.switchToState('down');
		//Send request to server
		nextStepCtrl.request();
	}
	window.setTimeout(function() { nextStepCtrl.resetState(); }, 500);
}

function pressUp() {
	if ( ifocus == null ) {
		return;
	}
	nextNumber(ifocus);//increase number
}

function pressDown() {
	if ( ifocus == null ) {
		return;
	}
	prevNumber(ifocus);//decrease number
	pushToNextStepButton();//Translate direction on next step button control
}

function onKeyDown(e) {
	if ( focusable == null ) {
		return;
	}
	keyCode = e.keyCode;
	if ( keyCode == 37 ) {//Prev focus
		moveFocusToPrev();
		return false;
	}
	if ( keyCode == 39 ) {//Next focus
		moveFocusToNext();
		return false;
	}
	if ( keyCode == 38 ) {//Up arrow
		pressUp();
		return false;
	}
	if ( keyCode == 40 ) {//Down arrow
		pressDown();
		return false;
	}
	if ( keyCode == 13 ) {//Down arrow
		pushToNextStepButton();
		return false;
	}
	//TODO: refactoring try unweave code
	if ( nextStepCtrl.isInFocus() ) {
		return false;
	}
	divId = focusable[ifocus];
	//Main keyboard
	if ( keyCode >= 48 && keyCode <= 57 ) {
		setInputNumber(divId, keyCode - 48);
		checkDuplicatesAndMaskIfFound(divId);
		return false;
	}
	//Additional keyboard
	if ( keyCode >= 96 && keyCode <= 105 ) {
		setInputNumber(divId, keyCode - 96);
		checkDuplicatesAndMaskIfFound(divId);
		return false;
	}
}


function afterPageLoad() {
	displayTimerInfo();
	displayImage('number1-img', 'asterisk');
	displayImage('number2-img', 'asterisk');
	displayImage('number3-img', 'asterisk');
	displayImage('number4-img', 'asterisk');
	displayImage('next-step-button-img', 'next-step-button-up');
	displayImage('bulls-count-img', '--b');
	displayImage('cows-count-img', '--c');
	displayImage('row-divider-img', 'logo-waves');
	//displayButton('next-step-button-img', 'next-step-button-sel');
	//displayButton('next-step-button-img', 'next-step-button-inv');
	//displayButton('next-step-button-img', 'next-step-button-down');
	makeFocusCycle(['number1', 'number2', 'number3', 'number4', 'next-step-button']);
	setTabIndex('time-start-text', 0);
	setTabIndex('game-duration-text', 0);
	setTabIndex('history-text', 0);
	setFocus(0, focusable);
}

/**
 * Run after load page. 
 */
document.addEventListener('DOMContentLoaded', afterPageLoad);
document.addEventListener('keydown', onKeyDown, false);
