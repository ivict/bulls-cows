function HTMLHttpRequest(myName, callback) {
	this.myName = myName;
	this.callback = callback;
	this.xmlhttp = null;
	this.iframe = null;

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
		if (xmlhttp.overrideMimeType) {
			xmlhttp.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	if (!xmlhttp) {
		if (document.createElement
				&& document.documentElement
				&& (window.opera || navigator.userAgent.indexOf('MSIE 5.0') == -1)) {
			var ifr = document.createElement('iframe');
			ifr.setAttribute('id', iframeID);
			ifr.setAttribute('name', iframeID);
			ifr.style.visibility = 'hidden';
			ifr.style.position = 'absolute';
			ifr.style.width = ifr.style.height = ifr.borderWidth = '0px';
			iframe = document.getElementsByTagName('body')[0].appendChild(ifr);
		} else if (document.body && document.body.insertAdjacentHTML) {
			document.body.insertAdjacentHTML('beforeEnd', '<iframe name="'
					+ iframeID + '" id="' + iframeID
					+ '" style="display:none"></iframe>');
		}
		if (window.frames && window.frames[iframeID])
			iframe = window.frames[iframeID];
		iframe.name = iframeID;
	}
	return this;
};

function createXMLHttpRequest() {
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	}
	if ( window.ActiveXObject ) {
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		}
		try {
			return new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {
		}
	}
	return null;
};
