<%@ page import="myself.invariants.*"%>
<%@ page import="myself.beans.*"%>
<%@ page import="myself.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=JspConsts.USER_PAGE_TITLE%></title>
</head>
<style type="text/css"><%@ include file="user.css"%></style>
<body>
	<script type="text/javascript">
		<%@ include file="org/json/json2.js"%>
		<%@ include file="com/stackoverflow/stuff.js"%>
		<%@ include file="org/ajaxpatterns/stuff.js"%>
	</script>
	<script type="text/javascript">
		/* Global variables */
		var requests=JSON.parse('<%=Serializer.toJSONString(Reflector.toMapStaticFields(Requests.class))%>');
		var messages=JSON.parse('<%=Serializer.toJSONString(Reflector.toMapStaticFields(Messages.class))%>');
		var user=JSON.parse('<%=Serializer.toJSONString(Reflector.toMap(User.class, (User) session.getAttribute(Strings.USER)))%>');
		var pageContextPath='<%=request.getContextPath()%>';
		<%@ include file="user.js"%>
	</script>
	<div class="no-borders user-board">
		<table class="no-borders">
			<tbody>
				<tr>
					<td/>
					<td id="form-field-central">

						<table class="form-field">
							<tbody>
								<tr><td class="user-name"><p id="user-name-txt"/></td><td colspan="2"><input type="text" id="user-name-editor"/></td></tr>
								<tr><td colspan="3"><img id="logo-img"/></td></tr>
								<tr><td colspan="3" class="game-description"><p id="game-description-txt"/></td></tr>
								<tr><td colspan="2" class="game-start-wider"/><td class="game-start"><input type="button" id="game-start-button"/></td></tr>
							</tbody>
						</table>
					</td>
					<td/>
				</tr>
			</tbody>
		</table>
	</div>
	<form id="game-start-request" method="post" class="game-start" accept-charset="ISO-8859-1">
		<input id="game-start-user-id" type="hidden"/>
		<input name="iehack" type="hidden" value="&#9760;"/>
	</form>	
</body>
</html>
