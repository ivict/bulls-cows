function displayImage(imgId, imageName) {
	img = document.getElementById(imgId);
	if ( img == null ) {
		return;
	}
	img.src = pageContextPath + "/images/" + imageName + ".png";
}

function setText(txtId, text) {
	txt = document.getElementById(txtId);
	if ( txt == null ) {
		return;
	}
	txt.innerHTML = text;
}

function setValue(inputId, value) {
	input = document.getElementById(inputId);
	if ( input == null ) {
		return;
	}
	input.value = value;
}

function isValidUserId(userId) {
	if ( userId != null && userId.match(/\S/)) {
		return true;
	}
	return false;
}

function afterPageLoad() {
	displayImage('logo-img', 'logo-cubics');
	setValue('game-start-button', messages.GAME_REPLAY_BUTTON);
	var userInTop10 = false;
	userName = user.name;
	top10Rate = document.getElementById("top-10-rate");
	for ( row in top10Rate.childNodes ) {
		var rateRow = top10Rate.childNodes[row];
		if ( !rateRow.childNodes ) {
			continue;
		}
		var name = rateRow.childNodes[0];
		if ( !name ) {
			continue;
		}
		//Name of user
		if ( name.innerHTML == userName ) {
			rateRow.className += ' selected-row';
			userInTop10 = true;
		}
	}
	if ( userName && !userInTop10) {
		setText('user-name-txt', user.name);
		setText('user-rate-value-txt', rate.ratingValue);
	}

	gameStartInput = document.getElementById('game-start-button');
	gameStartInput.onclick = function() {
		userId = user.name;
		//Sane check - double check
		if ( !isValidUserId(userId) ) {
			return;
		}
		userIdParam = document.getElementById('game-start-user-id');
		userIdParam.name = requests.USER_ID;
		userIdParam.value = userId;
		gameStartRequest = document.getElementById('game-start-request');
		gameStartRequest.action = requests.NEW_GAME;
		gameStartRequest.submit();
	};
	
	setText('rate-description-txt', messages.RATE_DESCRIPTION);
}

/**
 * Run after load page. 
 */
document.addEventListener('DOMContentLoaded', afterPageLoad);
