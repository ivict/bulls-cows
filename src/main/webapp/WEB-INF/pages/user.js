function displayImage(imgId, imageName) {
	img = document.getElementById(imgId);
	if ( img == null ) {
		return;
	}
	img.src = pageContextPath + "/images/" + imageName + ".png";
}

function setText(txtId, text) {
	txt = document.getElementById(txtId);
	if ( txt == null ) {
		return;
	}
	txt.innerHTML = text;
}

function setValue(inputId, value) {
	input = document.getElementById(inputId);
	if ( input == null ) {
		return;
	}
	input.value = value;
}

function setDisabled(inputId, disabled) {
	input = document.getElementById(inputId);
	if ( input == null ) {
		return;
	}
	input.disabled = disabled;
}

function isValidUserId(userId) {
	if ( userId != null && userId.match(/\S/)) {
		return true;
	}
	return false;
}

function afterPageLoad() {
	displayImage('logo-img', 'logo');
	setText('user-name-txt', messages.USER_NAME_HINT);
	setValue('game-start-button', messages.GAME_START_BUTTON);
	setText('game-description-txt', messages.GAME_DESCRIPTION);
	userName = user.name;
	if ( userName ) {
		setValue('user-name-editor', user.name);
	} else {
		setDisabled('game-start-button', true);
	}
	userNameInput = document.getElementById('user-name-editor');
	userNameInput.onmouseout = function() {
		setDisabled('game-start-button', !isValidUserId(this.value));
	};
	
	gameStartInput = document.getElementById('game-start-button');
	gameStartInput.onclick = function() {
		userNameEditor = document.getElementById('user-name-editor');
		if ( !userNameEditor ) {
			return;
		}
		userId = userNameEditor.value;
		//Sane check - double check
		if ( !isValidUserId(userId) ) {
			return;
		}
		userIdParam = document.getElementById('game-start-user-id');
		userIdParam.name = requests.USER_ID;
		userIdParam.value = userId;
		gameStartRequest = document.getElementById('game-start-request');
		gameStartRequest.action = requests.NEW_GAME;
		gameStartRequest.submit();
	};
}

/**
 * Run after load page. 
 */
document.addEventListener('DOMContentLoaded', afterPageLoad);
