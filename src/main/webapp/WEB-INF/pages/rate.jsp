<%@ page import="myself.invariants.*"%>
<%@ page import="myself.beans.*"%>
<%@ page import="myself.util.*"%>
<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=JspConsts.RATE_PAGE_TITLE%></title>
</head>
<style type="text/css"><%@ include file="rate.css"%></style>
<body>
	<script type="text/javascript">
		<%@ include file="org/json/json2.js"%>
		<%@ include file="com/stackoverflow/stuff.js"%>
		<%@ include file="org/ajaxpatterns/stuff.js"%>
	</script>
	<script type="text/javascript">
		/* Global variables */
		var requests=JSON.parse('<%=Serializer.toJSONString(Reflector.toMapStaticFields(Requests.class))%>');
		var messages=JSON.parse('<%=Serializer.toJSONString(Reflector.toMapStaticFields(Messages.class))%>');
		var user=JSON.parse('<%=Serializer.toJSONString(Reflector.toMap(User.class, (User) session.getAttribute(Strings.USER)))%>');
		var rate=JSON.parse('<%=Serializer.toJSONString(Reflector.toMap(Rate.class, ((User) session.getAttribute(Strings.USER)).getRate()))%>');
		var pageContextPath='<%=request.getContextPath()%>';
		<%@ include file="rate.js"%>
	</script>
	<div class="no-borders user-board">
		<table class="no-borders">
			<tbody>
				<tr>
					<td/>
					<td id="form-field-central">

						<table class="form-field no-borders">
							<tbody>
								<tr><td colspan=2 class="logo"><img id="logo-img"></td></tr>
								<tr>
									<td colspan="2">
										<table class="no-borders">
											<tbody id="top-10-rate">
												<%
													//Core tags lib not usage because can be generate troubles in deployment process
													List<Rate> top10 = (List<Rate>) request.getAttribute(Strings.TOP10);
													for ( Rate rate : top10 ) {
														User user = rate.getUser();
														out.append("<tr class=\"top-10-rate-row\">");
														out.append("<td class=\"top-10-rate-user-name\">").append(user.getName()).append("</td>");
														out.append("<td class=\"top-10-rate-user-rate-value\">").append(String.valueOf(rate.getRatingValue())).append("</td>");
														out.append("</tr>");
													}
												%>
											</tbody>
										</table>
									</td>
								</tr>
								<tr><td class="user-rate">
										<table class="no-borders">
											<tbody><tr><td class="user-name"><p id="user-name-txt"/></td><td class="user-rate-value"><p id="user-rate-value-txt"></td></tr></tbody>
										</table>
									</td>
									<td class="game-start"><input type="button" id="game-start-button"/></td>
								</tr>
							</tbody>
						</table>
					</td>
					<td/>
				</tr>
				<tr><td/><td class="rate-description"><p id="rate-description-txt"/></td><td/></tr>
			</tbody>
		</table>
	</div>
	<form id="game-start-request" method="post" class="game-start" accept-charset="ISO-8859-1">
		<input id="game-start-user-id" type="hidden"/>
		<input name="iehack" type="hidden" value="&#9760;"/>
	</form>	
</body>
</html>
