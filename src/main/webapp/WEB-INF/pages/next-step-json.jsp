<%@ page import="myself.invariants.*"%>
<%@ page import="myself.games.bac.*"%>
<%@ page import="myself.util.*"%>
<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	UserData userData = (UserData) session.getAttribute(Strings.USER_DATA);
	if ( userData.isGameOver() ) {
		return;
	}
	GameController gameController = new GameController();
	int number1 = Integer.valueOf(request.getParameter(Requests.NUMBER1));
	int number2 = Integer.valueOf(request.getParameter(Requests.NUMBER2));
	int number3 = Integer.valueOf(request.getParameter(Requests.NUMBER3));
	int number4 = Integer.valueOf(request.getParameter(Requests.NUMBER4));
	gameController.nextUserStep(userData, number1, number2, number3, number4);
%>
<%=Serializer.toJSONString(Reflector.toMap(UserData.class, (UserData) session.getAttribute(Strings.USER_DATA)))%>