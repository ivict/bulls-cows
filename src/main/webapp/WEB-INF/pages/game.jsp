<%@ page import="myself.invariants.*"%>
<%@ page import="myself.beans.*"%>
<%@ page import="myself.games.bac.*"%>
<%@ page import="myself.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=JspConsts.GAME_PAGE_TITLE%></title>
</head>
<style type="text/css"><%@ include file="game.css"%></style>
<body>
	<script type="text/javascript">
		<%@ include file="org/json/json2.js"%>
		<%@ include file="com/stackoverflow/stuff.js"%>
		<%@ include file="org/ajaxpatterns/stuff.js"%>
	</script>
	<script type="text/javascript">
		/* Global variables */
		var requests=JSON.parse('<%=Serializer.toJSONString(Reflector.toMapStaticFields(Requests.class))%>');
		var messages=JSON.parse('<%=Serializer.toJSONString(Reflector.toMapStaticFields(Messages.class))%>');
		var user=JSON.parse('<%=Serializer.toJSONString(Reflector.toMap(User.class, (User) session.getAttribute(Strings.USER)))%>');
		var userData=JSON.parse('<%=Serializer.toJSONString(Reflector.toMap(UserData.class, (UserData) session.getAttribute(Strings.USER_DATA)))%>');
		var pageContextPath='<%=request.getContextPath()%>';
		<%@ include file="game.js"%>
	</script>
	<div class="no-borders game-background">
		<table class="no-borders game-background">
			<tbody>
				<tr>
					<td id="blue-sky" />
				</tr>
				<tr>
					<td id="green-ground" />
				</tr>
			</tbody>
		</table>
	</div>
	<div class="no-borders game-board">
		<table class="no-borders">
			<tbody>
				<tr>
					<td/>
					<td id="game-field-central">

						<table class="think-borders game-field">
							<tbody>
								<tr><td colspan="7" class="think-borders top-margin"/></tr>
								<tr>
									<td rowspan="1" class="time-start"><p id="time-start-text"/></td>
									<td rowspan="2"><div id="number1" class="think-borders number-field"><img id="number1-img"/></div></td>
									<td rowspan="2"><div id="number2" class="think-borders number-field"><img id="number2-img"/></div></td>
									<td rowspan="2"><div id="number3" class="think-borders number-field"><img id="number3-img"/></div></td>
									<td rowspan="2"><div id="number4" class="think-borders number-field"><img id="number4-img"/></div></td>
									<td rowspan="4" class="col-divider"></td>
									<td rowspan="1" class="time-start"><p id="game-duration-text"/></td>
								</tr>
								<tr>
									<td rowspan="3"><div id="history" class="think-borders"><p id="history-text"/><%=Messages.HISTORY_HEADER%></div></td>
									<td rowspan="3"><div id="next-step-button" class="think-borders"><img id="next-step-button-img"></div></td>
								</tr>
								<tr><td colspan="4" class="think-borders row-divider"><img id="row-divider-img"/></td></tr>
								<tr>
									<td colspan="2"><div id="bulls-count" class="think-borders response-field"><img id="bulls-count-img"/></div></td>
									<td colspan="2"><div id="cows-count" class="think-borders response-field"><img id="cows-count-img"/></div></td>
								</tr>
							</tbody>
						</table>
					</td>
					<td/>
				</tr>
			</tbody>
		</table>
	</div>
	<form id="rate-request" method="post" class="rate" accept-charset="ISO-8859-1">
		<input id="rate-func"/>
		<input name="iehack" type="hidden" value="&#9760;"/>
	</form>	
</body>
</html>
