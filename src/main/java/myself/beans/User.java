
package myself.beans;

public class User {
	/**
	 * Unique value.
	 */
	private String name;
	
	/**
	 * Game stat.
	 */
	private Rate rate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Rate getRate() {
		return rate;
	}

	public void setRate(Rate rate) {
		this.rate = rate;
	}

}
