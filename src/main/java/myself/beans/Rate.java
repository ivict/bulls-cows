package myself.beans;

public class Rate {

	private User user;
	
	private long bestGameTimeInMillis;
	
	private int bestGameSteps;
	
	private int ratingValue;
	
	private int totalSteps;
	
	private int gamesCount;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getBestGameTimeInMillis() {
		return bestGameTimeInMillis;
	}

	public void setBestGameTimeInMillis(long bestGameTimeInMillis) {
		this.bestGameTimeInMillis = bestGameTimeInMillis;
	}

	public int getBestGameSteps() {
		return bestGameSteps;
	}

	public void setBestGameSteps(int bestGameSteps) {
		this.bestGameSteps = bestGameSteps;
	}

	public int getRatingValue() {
		return ratingValue;
	}

	public void setRatingValue(int ratingValue) {
		this.ratingValue = ratingValue;
	}

	public int getTotalSteps() {
		return totalSteps;
	}

	public void setTotalSteps(int totalSteps) {
		this.totalSteps = totalSteps;
	}

	public int getGamesCount() {
		return gamesCount;
	}

	public void setGamesCount(int gamesCount) {
		this.gamesCount = gamesCount;
	}
	
}
