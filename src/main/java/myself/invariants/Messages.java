package myself.invariants;

/**
 * Messages for logging and other purposes.
 */
public class Messages {
    private Messages() { }
    
    public static final String WARNING_MSG1 = "Disable persistence " +
        "for application, folder {0} not exists or not directory.";
    
    public static final String ERROR_MSG0 = "Unknown error";
    public static final String ERROR_MSG2 = "File not found.";
    public static final String ERROR_MSG3 = "General input/output error.";
    
    public static final String INVALID_SESSION = "User session not-valid.";
    
    /**
     * Html patterns.
     */
    public static final String HISTORY_ENTRY = "<br/><br/>{0}{1}{2}{3}<br/>ответ {4}Б{5}K";
    public static final String HISTORY_HEADER = "Ходы:";
    public static final String TIME_VALUE = "<b>Начало:</b><br/>{0}";
    public static final String GAME_DURATION_VALUE = "<b>Прошло</b><br/>времени:<br/>{0}";
    
    public static final String BROWSER_NOT_SUPPORTED = "Браузер не поддерживается!";
    
    public static final String USER_NAME_HINT = "Привет!<br/>Представьтесь, пожалуйста:";
    public static final String GAME_START_BUTTON = "Играть";
    public static final String GAME_DESCRIPTION = "Это логическая игра. Компьютер загадывает четырехзначное число, вы пытаетесь отгадать. Все цифры в числе различны. После каждой попытки, ответ возвращается в форме загадки из двух чисел - <b>число коров</b> и <b>число быков</b>. Число быков - количество правильно угаданных цифр с правильной позицией. Число коров - количество правильно угаданных цифр с неправильной позицией.<br/><br/>Количество попыток неограничено, но итог может повлиять на ваш рейтинг.<br/><br/><b>Пробуйте и выигрывайте! Удачи!</b>";
    public static final String GAME_REPLAY_BUTTON = "Переиграть";
    public static final String RATE_DESCRIPTION = "Число в рейтинге указывает на среднее число попыток. Если ваше имя присутствует в топе, то строка подсвечена.";
}
