package myself.invariants;

/**
 * JSP Pages constants.<br>
 * Later can be refactoring to Localization Engine.   
 */
public class JspConsts {
	private JspConsts() { }
	
	public static final String GAME_PAGE_TITLE = "Быки и Коровы.";
	public static final String USER_PAGE_TITLE = "Вход.";
	public static final String RATE_PAGE_TITLE = "Рейтинг.";
}
