package myself.invariants;

/**
 * String invariants. 
 */
public class Strings {
	private Strings() { }
	
	public static final String USER = "user";
	public static final String USER_DATA = "userData";
	public static final String NEW_USER_ID = "newUserId";
	
	public static final String TOP10 = "top10";
}
