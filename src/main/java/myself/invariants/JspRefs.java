package myself.invariants;

/**
 * Relative file-system references for jsps.<br/>
 * Internal server-side constants.  
 */
public class JspRefs {
	private JspRefs() { }

	public static final String USER_JSP = "/WEB-INF/pages/user.jsp";
	public static final String RATE_JSP = "/WEB-INF/pages/rate.jsp";
	public static final String GAME_JSP = "/WEB-INF/pages/game.jsp";
	public static final String NEXT_STEP_JSON_JSP = "/WEB-INF/pages/next-step-json.jsp";
}
