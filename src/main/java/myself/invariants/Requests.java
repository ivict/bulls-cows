package myself.invariants;

/**
 * Constants for client requests.<br/>
 * Direct mapping onto java script variable requests.
 */
public class Requests {
	//Request and him parameters
	public static final String GET_NEXT_STEP = "getNextStep";//Auto parameter
    public static final String NEXT_STEP = "game?" + GET_NEXT_STEP;
    public static final String NUMBER1 = "number1";
    public static final String NUMBER2 = "number2";
    public static final String NUMBER3 = "number3";
    public static final String NUMBER4 = "number4";
    
	public static final String GET_RATE = "getRate";//Auto parameter
    public static final String RATE = "enter?" + GET_RATE;

    public static final String NEW_GAME = "game";
	public static final String USER_ID = "userId";
}
