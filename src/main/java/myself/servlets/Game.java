package myself.servlets;

import static myself.invariants.JspRefs.GAME_JSP;
import static myself.invariants.JspRefs.NEXT_STEP_JSON_JSP;
import static myself.invariants.Requests.USER_ID;
import static myself.invariants.Strings.USER;
import static myself.invariants.Strings.USER_DATA;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myself.beans.User;
import myself.beans.Rate;
import myself.games.bac.UserData;
import myself.invariants.Requests;
import myself.services.UserLookupService;
import myself.services.UserMap;
import myself.util.Serializer;

/**
 * Main servlet. 
 */
public class Game extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		redirect(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		redirect(request, response);
	}
	
	private void redirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String address;
		if ( request.getParameter(Requests.GET_NEXT_STEP) == null) {
			String userId = request.getParameter(USER_ID);
			UserLookupService service = new UserMap();
			User user = service.findUser(userId);
			if ( user == null ) {
				//TODO: display additional help info
				user = new User();
				String userName = Serializer.convert(userId);
				user.setName(userName);
				Rate rate = new Rate();
				user.setRate(rate);
			}
			request.getSession().setAttribute(USER, user);
			//Start new Game
			request.getSession().setAttribute(USER_DATA, new UserData());
			address = GAME_JSP;
		} else {
			//Try to next step of Game
			address = NEXT_STEP_JSON_JSP;
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(address);
		dispatcher.forward(request, response);
	}

}
