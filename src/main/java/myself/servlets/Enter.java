package myself.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myself.beans.Rate;
import myself.beans.User;
import myself.games.bac.UserData;
import myself.invariants.Messages;
import myself.invariants.Requests;
import myself.invariants.Strings;
import myself.services.ReportsService;
import myself.services.UpdateRateService;
import myself.services.UserLookupService;
import myself.services.UserMap;
import myself.util.Reflector;

import static myself.invariants.Strings.*;
import static myself.invariants.Requests.*;
import static myself.invariants.JspRefs.*;

/**
 * DoorKeeper servlet implementation.<br>
 * Redirect to the new user if no cooks for last user-name.
 */
public class Enter extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(Reflector.class.getName());
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		redirect(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		redirect(request, response);
	}
	
	private void redirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String address;
		if ( request.getParameter(Requests.GET_RATE) == null) {
			//Go to registration form
			String userId = request.getParameter(USER_ID);
			UserLookupService service = new UserMap();
			User user = service.findUser(userId);
			request.setAttribute(USER, user);
			address = USER_JSP;
		} else {
			//Go to rate page
			UserData userData = (UserData) request.getSession().getAttribute(USER_DATA);
			User user = (User) request.getSession().getAttribute(USER);
			if ( userData == null || user == null ) {
				LOG.warning(Messages.INVALID_SESSION);
			} else {
				updateRate(userData, user);
			}
			//Generate top-10-report
			ReportsService service = new UserMap();
			List<Rate> top10 = service.top10Report();
			//request.getSession().setAttribute(USER, getFakeUser(10));
			//request.setAttribute(Strings.TOP10, getFakeTop10());
			request.setAttribute(Strings.TOP10, top10);
			address = RATE_JSP;
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(address);
		dispatcher.forward(request, response);
	}

	private User getFakeUser(int i) {
		User user = new User();
		user.setName("User" + i);
		Rate rate =  new Rate();
		rate.setRatingValue(i * 100);
		rate.setUser(user);
		user.setRate(rate);
		return user;
	}

	private List<Rate> getFakeTop10() {
		List<Rate> rates = new ArrayList<Rate>();
		for ( int i = 0; i < 10; i++ ) {
			User user = getFakeUser(i);
			rates.add(user.getRate());
		}
		return rates;
	}

	private void updateRate(UserData userData, User user) {
		Rate rate = user.getRate();
		rate.setGamesCount(rate.getGamesCount() + 1);
		rate.setTotalSteps(rate.getTotalSteps() + userData.getStepsCount());
		if ( rate.getBestGameSteps() > userData.getStepsCount() ) {
			rate.setBestGameSteps(userData.getStepsCount());
		}
		long gameDuration = System.currentTimeMillis() - userData.getStartTime(); 
		if ( rate.getBestGameTimeInMillis() > gameDuration ) {
			rate.setBestGameTimeInMillis(gameDuration);
		}
		rate.setRatingValue(rate.getTotalSteps() / rate.getGamesCount());
		UpdateRateService service = new UserMap();
		//Hack TODO: fix deserializing issue.
		rate.setUser(user);
		service.update(rate);
	}

}
