package myself.services;

import myself.beans.Rate;

public interface UpdateRateService {
	
	void update(Rate rate);
	
}
