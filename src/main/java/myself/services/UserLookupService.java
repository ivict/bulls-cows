package myself.services;

import myself.beans.User;

public interface UserLookupService {

	User findUser(String userId);

}
