package myself.services;

import java.util.List;

import myself.beans.Rate;

public interface ReportsService {
	List<Rate> top10Report();
}
