package myself.services;

import static myself.invariants.Messages.ERROR_MSG0;
import static myself.invariants.Messages.ERROR_MSG2;
import static myself.invariants.Messages.ERROR_MSG3;
import static myself.invariants.Messages.WARNING_MSG1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import myself.beans.Rate;
import myself.beans.User;
import myself.util.Reflector;
import myself.util.Serializer;

/**
 * Simple DAO for User and Statistics beans. 
 */
public class UserMap implements UserLookupService, UpdateRateService,
    ServletContextListener, ReportsService {
	
	private static final File STORAGE_DIR = new File(System.getProperty("user.home"), ".bulls-and-cows");
	private static final File usersProperties = new File(STORAGE_DIR, "users.properties");
	private static final Properties users = new Properties();
	
	private static final Logger LOG = Logger.getLogger(UserMap.class.getName());
	
	@Override
	public User findUser(String userId) {
		if ( userId == null ) {
			return null;
		}
		String stat = users.getProperty(userId);
		if ( stat == null ) {
			return null;
		}
		User user = new User();
		user.setName(userId);
		Rate rate = deserializeStat(stat);
		user.setRate(rate);
		rate.setUser(user);
		return user;
	}
	
	Rate deserializeStat(String stat) {
		Map<String, Object> map = Serializer.load(stat);
		return Reflector.newBean(Rate.class, map);
	}
	
	@Override
	public void update(Rate statistics) {
		User user = statistics.getUser();
		String stat = serializeStat(statistics);
		users.put(user.getName(), stat);
	}
	
	String serializeStat(Rate statistics) {
	    Map<String, Object> map = Reflector.toMap(
	        Rate.class, statistics);
	    return Serializer.toString(map);
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		stopService();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		startService();
	}

	private void startService() {
		STORAGE_DIR.mkdirs();
		if ( STORAGE_DIR.exists() && STORAGE_DIR.isDirectory() ) {
			try {
				FileInputStream input = new FileInputStream(usersProperties);
				users.load(input);
			} catch (FileNotFoundException e) {
				LOG.log(Level.SEVERE, ERROR_MSG2, e);
			} catch (IOException e) {
				LOG.log(Level.SEVERE, ERROR_MSG3, e);
			}
		} else {
			LOG.log(Level.WARNING, WARNING_MSG1, STORAGE_DIR);
		}
	}
	
	private void stopService() {
		FileOutputStream output;
		try {
			output = new FileOutputStream(usersProperties);
			users.store(output, null);
		} catch (FileNotFoundException e) {
			LOG.log(Level.SEVERE, ERROR_MSG2, e);
		} catch (IOException e) {
			LOG.log(Level.SEVERE, ERROR_MSG3, e);
		} catch (Throwable th) {
			LOG.log(Level.SEVERE, ERROR_MSG0, th);
		}
	}
	
	@Override
	public List<Rate> top10Report() {
		List<Rate> allUsers = new ArrayList<Rate>();
		for ( Map.Entry<Object, Object> e : users.entrySet() ) {
			String userId = (String) e.getKey();
			String stat = (String) e.getValue();
			User user = new User();
			user.setName(userId);
			Rate rate = deserializeStat(stat);
			user.setRate(rate);
			rate.setUser(user);
			allUsers.add(rate);
		}
		
		if ( allUsers.isEmpty() ) {
			return Collections.emptyList();
		}
		
		Collections.sort(allUsers, new Comparator<Rate>() {
			@Override
			public int compare(Rate o1, Rate o2) {
				return o1.getRatingValue() - o2.getRatingValue();
			}
		});
		
		return allUsers.subList(0, Math.min(allUsers.size(), 11));
	}

}
