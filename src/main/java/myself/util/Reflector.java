package myself.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static myself.invariants.Messages.*;

/**
 * Reflection utilities.<br>
 * Parse getters and setters.<br>
 * Key for map, always be in lower-case.
 */
public class Reflector {
	
	private static class BeanClassInfo {
		private Map<String, Method> getters;
		private Map<String, Method> setters;
	}
	
	private static final Map<Class<? extends Object>, BeanClassInfo> cache =
			new WeakHashMap<Class<? extends Object>, BeanClassInfo>();
	
	private static final Pattern GETTER = Pattern.compile("^(get|is)(\\w+)");
	private static final Pattern SETTER = Pattern.compile("^set(\\w+)");
	
	private static final Lock lockOfCache = new ReentrantLock();
	
	private static final Logger LOG = Logger.getLogger(Reflector.class.getName());
	
	private Reflector() { }

	public static <T> T newBean(Class<T> beanClass, Map<String, Object> beanProps) {
		BeanClassInfo ci = getBeanInfo(beanClass);
		T bean = newInstance(beanClass);
		for ( Map.Entry<String, Object> e : beanProps.entrySet() ) {
			Method setter = ci.setters.get(e.getKey());
			if (setter == null) {
				continue;
			}
			setValue(bean, setter, e.getValue());
		}
		return bean;
	}
	
	public static <T> Map<String, Object> toMap(Class<T> beanClass, T bean) {
		if ( bean == null ) {
			return Collections.emptyMap();
		}
		BeanClassInfo bci = getBeanInfo(beanClass);
		Map<String, Object> map = new HashMap<String, Object>();
		for ( Map.Entry<String, Method> e : bci.getters.entrySet() ) {
			Method getter = e.getValue();
			map.put(e.getKey(), getValue(bean, getter));
		}
		return compact(map);
	}
	
	private static <T> T newInstance(Class<T> beanClass) {
		try {
			return beanClass.newInstance();
		} catch (InstantiationException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		} catch (IllegalAccessException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		}
		return null;
	}
	
	private static <T> void setValue(T bean, Method method, Object value) {
		try {
			method.invoke(bean, value);
		} catch (IllegalArgumentException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		} catch (IllegalAccessException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		} catch (InvocationTargetException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		}
	}
	
	private static Object getValue(Object bean, Method method) {
		try {
			return method.invoke(bean);
		} catch (IllegalArgumentException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		} catch (IllegalAccessException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		} catch (InvocationTargetException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		}
		return null;
	}
	
	private static<T> BeanClassInfo getBeanInfo(Class<T> beanClass) {
		BeanClassInfo ci = cache.get(beanClass);
		if ( ci != null ) {
			return ci;
		}
		try {
			lockOfCache.lock();
			ci = new BeanClassInfo();
			HashMap<String, Method> getters = new HashMap<String, Method>();
			HashMap<String, Method> setters = new HashMap<String, Method>();
			introspection(beanClass, getters, setters);
			ci.getters = compact(getters);
			ci.setters = compact(setters);
			cache.put(beanClass, ci);
		} finally {
			lockOfCache.unlock();
		}
		return ci;
	}

	private static <K, V> Map<K, V> compact(Map<K, V> map) {
		if ( map.isEmpty() ) {
			return Collections.emptyMap();
		}
		if ( map.size() == 1 ) {
			Map.Entry<K, V> e = map.entrySet().iterator().next();
			return Collections.singletonMap(e.getKey(), e.getValue());
		}
		return map;
	}

	private static<T> void introspection(Class<T> beanClass,
			HashMap<String, Method> getters, HashMap<String, Method> setters) {
		for ( Method m : beanClass.getMethods() ) {
			//Only public not final setters and getters for bean.
			if ( !Modifier.isPublic(m.getModifiers())
					|| Modifier.isFinal(m.getModifiers())) {
				continue;
			}
			Matcher matcher = GETTER.matcher(m.getName());
			if ( matcher.matches() ) {
				String propertyName = matcher.group(2);
				String key = formatToBeanProperty(propertyName);
				getters.put(key, m);
			}
			matcher = SETTER.matcher(m.getName());
			if ( matcher.matches() ) {
				String propertyName = matcher.group(1);
				String key = formatToBeanProperty(propertyName);
				setters.put(key, m);
			}
		}
	}

	private static String formatToBeanProperty(String text) {
		StringBuilder sb = new StringBuilder(text);
		sb.setCharAt(0, Character.toLowerCase(sb.charAt(0)));
		String propertyName = sb.toString();
		return propertyName;
	}
	
	/**
	 * Addon for constants migration into JSON.  
	 */

	private static class StaticClassInfo {
		private Map<String, Field> finalPublicFields;
	}
	
	private static final Map<Class<?>, StaticClassInfo> cacheOfConsts =
			new WeakHashMap<Class<?>, StaticClassInfo>();
	private static final Lock lockOfCacheOfConsts = new ReentrantLock();
	
	private static Object getValue(Object object, Field field) {
		try {
			return field.get(object);
		} catch (IllegalArgumentException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		} catch (IllegalAccessException e) {
			LOG.log(Level.SEVERE, ERROR_MSG0, e);
		}
		return null;
	}
	
	private static StaticClassInfo getStaticInfo(Class<?> objectClass) {
		StaticClassInfo sci = cacheOfConsts.get(objectClass);
		if ( sci != null ) {
			return sci;
		}
		try {
			lockOfCacheOfConsts.lock();
			sci = new StaticClassInfo();
			HashMap<String, Field> finalPublicStaticFields = new HashMap<String, Field>();
			introspectionOfPublicFinalStaticFields(objectClass, finalPublicStaticFields);
			sci.finalPublicFields = compact(finalPublicStaticFields);
			cacheOfConsts.put(objectClass, sci);
		} finally {
			lockOfCacheOfConsts.unlock();
		}
		return sci;
	}

	private static<T> void introspectionOfPublicFinalStaticFields(Class<?> objectClass,
			HashMap<String, Field> publicFinalStaticFields) {
		for ( Field f : objectClass.getFields() ) {
			//Only public not final setters and getters for bean.
			int mods = f.getModifiers();
			if ( !Modifier.isPublic(mods)
					|| !Modifier.isFinal(mods)
					|| !Modifier.isStatic(mods)) {
				continue;
			}
			String key = f.getName();
			publicFinalStaticFields.put(key, f);
		}
	}
	
	public static Map<String, Object> toMapStaticFields(Class<?> objectClass) {
		StaticClassInfo sci = getStaticInfo(objectClass);
		Map<String, Object> map = new HashMap<String, Object>();
		for ( Map.Entry<String, Field> e : sci.finalPublicFields.entrySet() ) {
			Field finalStatic = e.getValue();
			map.put(e.getKey(), getValue(null, finalStatic));
		}
		return compact(map);
	}
	
}
