package myself.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

/**
 * Serialize utilities. 
 */
public class Serializer {
	
	private Serializer() { }
	
	public static String toString(Map<String, Object> map) {
		//Sort by keys. 
		List<Map.Entry<String, Object>> entries = new ArrayList<Map.Entry<String,Object>>(map.entrySet());
		Collections.sort(entries, new Comparator<Entry<String, Object>>() {
			@Override
			public int compare(Entry<String, Object> o1,
					Entry<String, Object> o2) {
				return o1.getKey().compareToIgnoreCase(o2.getKey());
			}
		});
		Iterator<Map.Entry<String, Object>> i = entries.iterator();
		if ( !i.hasNext() ) {
			return "{}";
		}
		StringBuilder strmkr = new StringBuilder();
		strmkr.append('{');
		for (;;) {
			Entry<String, Object> e = i.next();
			strmkr.append(e.getKey());
			strmkr.append('=');
			strmkr.append(quot(e.getValue()));
			if ( !i.hasNext() ) {
				strmkr.append('}');
				return strmkr.toString();
			}
			strmkr.append(',');
		}
	}
	
	private static String quot(Object value) {
		if ( value == null ) {
			return "";
		}
		Class<? extends Object> classOfValue = value.getClass();
		//String value
		if ( CharSequence.class.isAssignableFrom(classOfValue) ) {
			StringBuilder strmkr = new StringBuilder();
			strmkr.append('"');
			CharSequence sequence = (CharSequence) value;
			for ( int i = 0; i < sequence.length(); i++ ) {
				char c = sequence.charAt(i);
				if ( c == '"' ) {
					strmkr.append('\\');
				}
				strmkr.append(c);
			}
			strmkr.append('"');
			return strmkr.toString();
		}
		//Number value
		if ( Number.class.isAssignableFrom(classOfValue) ) {
			return String.valueOf(value);
		}
		//Boolean value
		if ( Boolean.class.isAssignableFrom(classOfValue) ) {
			return String.valueOf(value);
		}
		//Character value
		if ( Character.class.isAssignableFrom(classOfValue) ) {
			StringBuilder strmkr = new StringBuilder();
			strmkr.append('\'').append((Character) value).append('\'');
			return strmkr.toString();
		}
		//Primitive type
		if ( classOfValue.isPrimitive() ) {
			return String.valueOf(value);
		}
		return "";//null value after deserialize.
	}
	
	private static class KeyValueToken {
		static final Pattern QUOTED = Pattern.compile("([\\p{Alpha}_][\\p{Alnum}_]*)\\s*=\\s*\"(.*)\"\\s*,");
		static final Pattern QUOTED_LAST = Pattern.compile("([\\p{Alpha}_][\\p{Alnum}_]*)\\s*=\\s*\"(.*)\"\\s*\\}");
		static final Pattern NOT_QUOTED = Pattern.compile("([\\p{Alpha}_][\\p{Alnum}_]*)\\s*=\\s*([^\\,]*)\\s*,");
		static final Pattern NOT_QUOTED_LAST = Pattern.compile("([\\p{Alpha}_][\\p{Alnum}_]*)\\s*=\\s*([^\\,]*)\\s*\\}");
		
		static final Pattern CHARACTER = Pattern.compile("'\\?.'");
		
		final boolean hasNext;
		final boolean quoted;
		
		final String key;
		final String value;
		
		/**
		 * Create token for key, value pair and move position in scanner.
		 * @param scanner
		 */
		public KeyValueToken(Scanner scanner) {
			String token;
			token = scanner.findInLine(QUOTED);
			if ( token != null ) {
				hasNext = true;
				quoted = true;
				MatchResult m = scanner.match();
				key = m.group(1);
				value = m.group(2);
				return;
			}
			token = scanner.findInLine(NOT_QUOTED);
			if ( token != null ) {
				hasNext = true;
				quoted = false;
				MatchResult m = scanner.match();
				key = m.group(1);
				value = m.group(2);
				return;
			}
			token = scanner.findInLine(QUOTED_LAST);
			if ( token != null ) {
				hasNext = false;
				quoted = true;
				MatchResult m = scanner.match();
				key = m.group(1);
				value = m.group(2);
				return;
			}
			token = scanner.findInLine(NOT_QUOTED_LAST);
			if ( token != null ) {
				hasNext = true;
				quoted = false;
				MatchResult m = scanner.match();
				key = m.group(1);
				value = m.group(2);
				return;
			}
			//Stop parse.
			hasNext = false;
			quoted = false;
			key = null;
			value = null;
		}

	}

	public static Map<String, Object> load(String text) {
		Scanner scanner = new Scanner(text);
		scanner.skip("\\{");
		KeyValueToken token = new KeyValueToken(scanner);
		if ( !token.hasNext ) {
			return Collections.emptyMap();
		}
		Map<String, Object> map = new HashMap<String, Object>();
		
		Object value = recognizeType(token.value, token.quoted);
		map.put(token.key, value);
		while ( token.hasNext ) {
			token = new KeyValueToken(scanner);
			value = recognizeType(token.value, token.quoted);
			map.put(token.key, value);
		}
		return map;
	}

	static Object recognizeType(String value, boolean quoted) {
		if ( value == null ) {
			return null;
		}
		//Null value detected.
		if ( value.isEmpty() && !quoted ) {
			return null;
		}
		//String detected.
		if ( quoted ) {
			return value;
		}
		//Character detected.
		if ( KeyValueToken.CHARACTER.matcher(value).matches() ) {
			if ( value.length() == 1 ) {
				return value.charAt(0);
			}
			return String.valueOf("\\" + value.charAt(1)).charAt(0);
		}
		Scanner scanner = new Scanner(value).useLocale(Locale.ENGLISH);
		if ( scanner.hasNextBoolean() ) {
			return Boolean.valueOf(value);
		}
		if ( scanner.hasNextByte() ) {
			return Byte.valueOf(value);
		}
		if ( scanner.hasNextShort() ) {
			return Short.valueOf(value);
		}
		if ( scanner.hasNextInt() ) {
			return Integer.valueOf(value);
		}
		if ( scanner.hasNextLong() ) {
			return Long.valueOf(value);
		}
		if ( scanner.hasNextBigInteger() ) {
			return scanner.nextBigInteger();
		}
		//Float, Double and BigDecimal recognize
		if ( scanner.hasNextFloat() ) {
			return Float.valueOf(value);
		}
		if ( scanner.hasNextDouble() ) {
			return Double.valueOf(value);
		}
		if ( scanner.hasNextBigDecimal() ) {
			return scanner.nextBigDecimal();
		}
		return null;
	}

	/**
	 * JSON Stuff.
	 */
	public static String toJSONString(Map<String, Object> map) {
		Iterator<Map.Entry<String, Object>> i = map.entrySet().iterator();
		if ( !i.hasNext() ) {
			return "{}";
		}
		StringBuilder strmkr = new StringBuilder();
		strmkr.append('{');
		for (;;) {
			Entry<String, Object> e = i.next();
			strmkr.append('"').append(e.getKey()).append('"');
			strmkr.append(':');
			String value = quot(e.getValue());
			if ( !value.isEmpty() ) {
				strmkr.append(value);
			} else {
				strmkr.append("\"\"");//Null value for javascript;
			}
			if ( !i.hasNext() ) {
				strmkr.append('}');
				return strmkr.toString();
			}
			strmkr.append(',');
		}
	}

	private final static Pattern UTF_SYMBOL = Pattern.compile("\\&\\#\\d\\d\\d\\d\\;");
	public static String convert(String input) {
		//ISO-8859-1 Encoding detection
		Matcher matcher = UTF_SYMBOL.matcher(input);
		if ( matcher.find() ) {
			int startPos = matcher.start();
			int endPos = matcher.end();
			String symbol = input.substring(startPos, endPos);
			int code = Integer.valueOf(symbol.substring(2, 6));
			StringBuilder output = new StringBuilder(input.length());
			output.append((char) code);
			while ( matcher.find(endPos) ) {
				startPos = matcher.start();
				endPos = matcher.end();
				symbol = input.substring(startPos, endPos);
				code = Integer.valueOf(symbol.substring(2, 6));
				output.append((char) code);
			}
			return output.toString();
		}
		return input;
	}

}
