package myself.games.bac;

import java.util.BitSet;

/**
 * Controller for Bulls&Cows. 
 */
public class GameController {

	/**
	 * Game begin.
	 * @return initial game state.
	 */
	public UserData start() {
		return new UserData();
	}
	
	/**
	 * Next step of user.<br/>
	 * Data will be changed, after correct user input.<br/>
	 * If input data invalid for game, throw IllegalStateException.<br/>
	 * If game over, nothing do.
	 * @param data game state.
	 * @param number1 input user number in first position.
	 * @param number2 input user number in second position.
	 * @param number3 input user number in third position.
	 * @param number4 input user number in forth position.
	 */
	public void nextUserStep(UserData data, int number1, int number2,
			int number3, int number4) {
		throwIfBadUserInput(number1, number2, number3, number4);
		if ( data.isGameOver() ) {
			return;
		}
		putUserInput(data, number1, number2, number3, number4);
	}
	
	void putUserInput(UserData data, int number1, int number2,
			int number3, int number4) {
		data.number1 = (byte) number1;
		data.number2 = (byte) number2;
		data.number3 = (byte) number3;
		data.number4 = (byte) number4;
		byte cowsCount = 0;
		byte bullsCount = 0;
		BitSet inputIndex = mkInputIndex(data);
		for ( int i = 1; i <= 4; i++ ) {
			byte number = numberAtPos(data, i);
			byte secret = secretAtPos(data, i);
			//Check bulls
			if ( number == secret ) {
				bullsCount++;
				continue;
			}
			//Check cows
			if ( inputIndex.get(secret) ) {
				cowsCount++;
			}
		}
		data.bullsCount = bullsCount;
		data.cowsCount = cowsCount;
		data.stepsCount++;
	}

	byte secretAtPos(UserData ud, int i) {
		switch( i ) {
		case 1:
			return ud.secretNumber1;
		case 2:
			return ud.secretNumber2;
		case 3:
			return ud.secretNumber3;
		case 4:
			return ud.secretNumber4;
		}
		throw new IllegalStateException();
	}

	private byte numberAtPos(UserData ud, int i) {
		switch( i ) {
		case 1:
			return ud.number1;
		case 2:
			return ud.number2;
		case 3:
			return ud.number3;
		case 4:
			return ud.number4;
		}
		throw new IllegalStateException();

	}

	private BitSet mkInputIndex(UserData ud) {
		BitSet iidx = new BitSet();
		iidx.set(ud.number1);
		iidx.set(ud.number2);
		iidx.set(ud.number3);
		iidx.set(ud.number4);
		return iidx;
	}
	
	private void throwIfBadUserInput(int n1, int n2, int n3, int n4) {
		if ( isBadRange(n1) ||  n1 == n2 || n1 == n3 || n1 == n4 ) {
			throw new IllegalStateException();
		}
		if ( isBadRange(n2) ||  n2 == n3 || n2 == n4 ) {
			throw new IllegalStateException();
		}
		if ( isBadRange(n3) || n3 == n4 ) {
			throw new IllegalStateException();
		}
		if ( isBadRange(n4) ) {
			throw new IllegalStateException();
		}
	}

	private boolean isBadRange(int n) {
		return n < 0 || n > 9;
	}

}
