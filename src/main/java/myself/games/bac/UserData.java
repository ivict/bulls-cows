package myself.games.bac;

import java.util.BitSet;
import java.util.Random;

/**
 * Model for game Bulls & Cows. 
 */
public class UserData {
    final long startTime = System.currentTimeMillis();
    int stepsCount;
    
    /* Secret numbers */
    final byte secretNumber1 = nextNumber();
    final byte secretNumber2 = nextNumber(secretNumber1);
    final byte secretNumber3 = nextNumber(secretNumber1, secretNumber2);
    final byte secretNumber4 = nextNumber(secretNumber1, secretNumber2, secretNumber3);
    
    /* Response */
    byte cowsCount;
    byte bullsCount;
    
    /* Input */
    byte number1;
    byte number2;
    byte number3;
    byte number4;
    
    final Random RAND = new Random();
    
    byte nextNumber() {
    	return (byte) RAND.nextInt(10);
    }
    
    byte nextNumber(Byte... excludes) {
    	BitSet bsi = new BitSet();
    	for ( Byte ex : excludes ) {
    		bsi.set(ex);
    	}
    	int n = 10 - bsi.cardinality();
    	if ( n <= 0 ) {
    		throw new IllegalStateException();
    	}
    	//Stop index for includes numbers
    	int s = RAND.nextInt(n);
    	int position = 0;
    	//Search next include number and stop after s
    	for ( int i = 0; i <= s; i++ ) {
    		int nextIndex = bsi.nextClearBit(position);
    		position = nextIndex + 1;
    	}
    	return (byte) (position - 1);
    }

    /**
     * Start time of game.
     * @return milliseconds since 1 Jan 1970.
     */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * Count of user steps.
	 * @return positive integer value.
	 */
	public int getStepsCount() {
		return stepsCount;
	}

	/**
	 * Число знаков угаданных игроком, в неправильных позициях.
	 * @return положительное целое.
	 */
	public byte getCowsCount() {
		return cowsCount;
	}

	/**
	 * Число знаков угаданных игроком, чьи позиции совпадают.
	 * @return положительное число.
	 */
	public byte getBullsCount() {
		return bullsCount;
	}
	
	/**
	 * Indicate game over or not.
	 * @param data game state.
	 * @return true if game state final.
	 */
	public boolean isGameOver() {
		if ( number1 != secretNumber1 ) {
			return false;
		}
		if ( number2 != secretNumber2 ) {
			return false;
		}
		if ( number3 != secretNumber3 ) {
			return false;
		}
		if ( number4 != secretNumber4 ) {
			return false;
		}
		return true;
	}

	/**
	 * Last user number in first position.  
	 * @return positive number
	 */
	public byte getNumber1() {
		return number1;
	}

	/**
	 * Last user number in second position.  
	 * @return positive number
	 */
	public byte getNumber2() {
		return number2;
	}

	/**
	 * Last user number in third position.  
	 * @return positive number
	 */
	public byte getNumber3() {
		return number3;
	}

	/**
	 * Last user number in forth position.  
	 * @return positive number
	 */
	public byte getNumber4() {
		return number4;
	}
    
}
