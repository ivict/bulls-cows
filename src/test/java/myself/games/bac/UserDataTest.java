package myself.games.bac;

import static org.junit.Assert.*;

import org.junit.Test;

public class UserDataTest {

	@Test
	public void testNextNumber() {
		UserData ud = new UserData();
		byte nine = ud.nextNumber((byte) 0, (byte) 1, (byte) 2,
				(byte) 3, (byte) 4, (byte) 5, (byte) 6, (byte) 7,
				(byte) 8);
		assertEquals(9, nine);
	}

	@Test
	public void testNextNumberException() {
		UserData ud = new UserData();
		try {
			ud.nextNumber((byte) 0, (byte) 1,	(byte) 2,
					(byte) 3, (byte) 4, (byte) 5, (byte) 6, (byte) 7,
					(byte) 8, (byte) 9);
			assertTrue(false);
		} catch (IllegalStateException ex) {
			assertTrue(true);
		}
	}

}
