package myself.games.bac;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameControllerTest {

	@Test
	public void testStart() {
		GameController gameCtrl = new GameController();
		UserData data = gameCtrl.start();
		assertNotSame(data.secretNumber1, data.secretNumber2);
		assertNotSame(data.secretNumber1, data.secretNumber3);
		assertNotSame(data.secretNumber1, data.secretNumber4);
		assertNotSame(data.secretNumber2, data.secretNumber3);
		assertNotSame(data.secretNumber2, data.secretNumber4);
		assertNotSame(data.secretNumber3, data.secretNumber4);
	}

	@Test
	public void testNextUserStep() {
		GameController gameCtrl = new GameController();
		UserData data = gameCtrl.start();
		int bullsTotal = 0;
		int cowsTotal = 0;
		gameCtrl.nextUserStep(data, 0, 1, 2, 3);
		bullsTotal += data.bullsCount;
		cowsTotal += data.cowsCount;
		gameCtrl.nextUserStep(data, 4, 5, 6, 7);
		bullsTotal += data.bullsCount;
		cowsTotal += data.cowsCount;
		gameCtrl.nextUserStep(data, 8, 9, 0, 1);
		bullsTotal += data.bullsCount;
		cowsTotal += data.cowsCount;
		assertEquals(3, data.stepsCount);
		assertTrue(bullsTotal > 0 || cowsTotal > 0);
	}

	@Test
	public void testIsGameOver() {
		GameController gameCtrl = new GameController();
		UserData data = gameCtrl.start();
		assertFalse(data.isGameOver());
		data.number1 = data.secretNumber1;
		data.number2 = data.secretNumber2;
		data.number3 = data.secretNumber3;
		data.number4 = data.secretNumber4;
		assertTrue(data.isGameOver());
	}
	
	@Test
	public void testGame() {
		UserData ud = new UserData();
		GameController gameCtrl = new GameController();
		UserData data = gameCtrl.start();
		//Step 1
		int n2 = data.secretNumber1;
		int n1 = ud.nextNumber((byte) n2, data.secretNumber1, data.secretNumber2, data.secretNumber3, data.secretNumber4);
		int n3 = ud.nextNumber((byte) n2, (byte) n1, data.secretNumber1, data.secretNumber2, data.secretNumber3, data.secretNumber4);
		int n4 = ud.nextNumber((byte) n2, (byte) n1, (byte) n3, data.secretNumber1, data.secretNumber2, data.secretNumber3, data.secretNumber4);
		gameCtrl.nextUserStep(data, n1, n2, n3, n4);
		assertFalse(data.isGameOver());
		assertEquals(0, data.getBullsCount());
		assertEquals(1, data.getCowsCount());
		assertEquals(1, data.getStepsCount());
		
		//Step 2
		n1 = data.secretNumber1;
		n2 = ud.nextNumber((byte) n1, data.secretNumber1, data.secretNumber2, data.secretNumber3, data.secretNumber4);
		n3 = ud.nextNumber((byte) n2, (byte) n1, data.secretNumber1, data.secretNumber2, data.secretNumber3, data.secretNumber4);
		n4 = ud.nextNumber((byte) n2, (byte) n1, (byte) n3, data.secretNumber1, data.secretNumber2, data.secretNumber3, data.secretNumber4);
		gameCtrl.nextUserStep(data, n1, n2, n3, n4);
		assertFalse(data.isGameOver());
		assertEquals(1, data.getBullsCount());
		assertEquals(0, data.getCowsCount());
		assertEquals(2, data.getStepsCount());
		
		//Step 3
		n1 = data.secretNumber1;
		n2 = data.secretNumber2;
		n3 = data.secretNumber3;
		n4 = data.secretNumber4;
		gameCtrl.nextUserStep(data, n1, n2, n3, n4);
		assertTrue(data.isGameOver());
		assertEquals(4, data.getBullsCount());
		assertEquals(0, data.getCowsCount());
		assertEquals(3, data.getStepsCount());
		
		//Game over is game over
		n4 = data.secretNumber1;
		n1 = data.secretNumber4;
		gameCtrl.nextUserStep(data, n1, n2, n3, n4);
		assertTrue(data.isGameOver());
		assertEquals(4, data.getBullsCount());
		assertEquals(0, data.getCowsCount());
		assertEquals(3, data.getStepsCount());
		
	}

	@Test
	public void testFromDescription() {
		final int n1 = 7;
		final int n2 = 3;
		final int n3 = 2;
		final int n4 = 8;
		GameController gameCtrl = new GameController() {
			@Override
			byte secretAtPos(UserData ud, int i) {
				switch ( i ) {
				case 1:
					return n1;
				case 2:
					return n2;
				case 3:
					return n3;
				case 4:
					return n4;
				}
				throw new IllegalStateException();
			}
		};
		
		UserData data = gameCtrl.start();

		gameCtrl.nextUserStep(data, 0, 8, 1, 9);
		assertEquals(data.getBullsCount(), 0);
		assertEquals(data.getCowsCount(), 1);

		gameCtrl.nextUserStep(data, 4, 0, 7, 3);
		assertEquals(data.getBullsCount(), 0);
		assertEquals(data.getCowsCount(), 2);
		
		gameCtrl.nextUserStep(data, 5, 8, 2, 0);
		assertEquals(data.getBullsCount(), 1);
		assertEquals(data.getCowsCount(), 1);
		
		gameCtrl.nextUserStep(data, 3, 4, 2, 9);
		assertEquals(data.getBullsCount(), 1);
		assertEquals(data.getCowsCount(), 1);
		
		gameCtrl.nextUserStep(data, 5, 9, 6, 0);
		assertEquals(data.getBullsCount(), 0);
		assertEquals(data.getCowsCount(), 0);

		gameCtrl.nextUserStep(data, 7, 2, 3, 8);
		assertEquals(data.getBullsCount(), 2);
		assertEquals(data.getCowsCount(), 2);

		gameCtrl.nextUserStep(data, 7, 3, 2, 8);
		assertEquals(data.getBullsCount(), 4);
		assertEquals(data.getCowsCount(), 0);
	}
}
