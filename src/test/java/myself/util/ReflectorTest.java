package myself.util;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class ReflectorTest {

	public static class BeanClass0 {
	}
	
	public static class BeanClass1 {
		private long key1;

		public long getKey1() {
			return key1;
		}

		public void setKey1(long key1) {
			this.key1 = key1;
		}
	}

	public static class BeanClass2 {
		private long key1;
		private String key2;

		public long getKey1() {
			return key1;
		}

		public void setKey1(long key1) {
			this.key1 = key1;
		}

		public String getKey2() {
			return key2;
		}

		public void setKey2(String key2) {
			this.key2 = key2;
		}
		
	}

	@Test
	public void testToMap() {
		Map<String, Object> map0 =
				Reflector.toMap(BeanClass0.class, new BeanClass0());
		assertNotNull(map0);
		assertEquals(0, map0.size());
		
		BeanClass1 bean1 = new BeanClass1();
		bean1.setKey1(Long.MAX_VALUE);
		Map<String, Object> map1 =
				Reflector.toMap(BeanClass1.class, bean1);
		assertNotNull(map1);
		assertEquals(1, map1.size());
		assertEquals(Long.MAX_VALUE, map1.get("key1"));
		
		BeanClass2 bean2 = new BeanClass2();
		bean2.setKey1(Long.MIN_VALUE);
		bean2.setKey2("value");
		Map<String, Object> map2 =
				Reflector.toMap(BeanClass2.class, bean2);
		assertNotNull(map2);
		assertEquals(2, map2.size());
		assertEquals(Long.MIN_VALUE, map2.get("key1"));
		assertEquals("value", map2.get("key2"));
	}

	@Test
	public void testNewBean() {
		BeanClass0 bean0 = Reflector.newBean(BeanClass0.class, Collections.<String, Object>emptyMap());
		assertNotNull(bean0);
		
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("key1", -1);
		BeanClass1 bean1 = Reflector.newBean(BeanClass1.class, map1);
		assertNotNull(bean1);
		assertEquals(-1, bean1.getKey1());
		
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("key1", 1);
		map2.put("key2", "value");
		BeanClass2 bean2 = Reflector.newBean(BeanClass2.class, map2);
		assertNotNull(bean2);
		assertEquals(1, bean2.getKey1());
		assertEquals("value", bean2.getKey2());
	}
	
	public static class Constants {
		public static final String ITS_A_STRING = "It's a string";
		public static final int ZERO = 0;
		public final int ONE = 1;
		public String ITS_STRING_TOO = "It's a string too";
		static final int THREE = 3;
	}
	
	@Test
	public void testToMapStaticFields() {
		Map<String, Object> map2 =
				Reflector.toMapStaticFields(Constants.class);
		assertNotNull(map2);
		assertEquals(2, map2.size());
		
		assertEquals(Constants.ITS_A_STRING, map2.get("ITS_A_STRING"));
		assertEquals(Constants.ZERO, map2.get("ZERO"));
	}


}
