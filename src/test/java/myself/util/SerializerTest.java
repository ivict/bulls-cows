package myself.util;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class SerializerTest {

	@Test
	public void testLoad() {
		Map<String, Object> map = Serializer.load("{}");
		assertNotNull(map);
		assertEquals(0, map.size());
		
		Map<String, Object> map2 = Serializer.load("{key1=0,key2=\"value\"}");
		assertNotNull(map2);
		assertEquals(0, ((Number) map2.get("key1")).intValue());
		assertEquals("value", map2.get("key2"));

		Map<String, Object> map3 = Serializer.load("{key1=true,key2=12.5}");
		assertNotNull(map3);
		assertEquals(true, map3.get("key1"));
		assertEquals(12.5, ((Number) map3.get("key2")).doubleValue(), 0.01);
		
		Map<String, Object> map4 = Serializer.load("{bestGameSteps=0,bestGameTimeInMillis=0,gamesCount=1,ratingValue=0,totalSteps=0,user=}");
		assertNotNull(map4);
		assertEquals(0, ((Number) map4.get("bestGameSteps")).intValue());
		assertEquals(0, ((Number) map4.get("bestGameTimeInMillis")).intValue());
		assertEquals(1, ((Number) map4.get("gamesCount")).intValue());
		assertEquals(0, ((Number) map4.get("ratingValue")).intValue());
		assertEquals(0, ((Number) map4.get("totalSteps")).intValue());
		assertNull(map4.get("user"));
		
	}

	@Test
	public void testToString() {
		String text = Serializer.toString(Collections.<String, Object>emptyMap());
		assertNotNull(text);
		assertEquals("{}", text);
		
		Map<String, Object> mmap2 = new HashMap<String, Object>();
		mmap2.put("key1", 0);
		mmap2.put("key2", "value");
		String text2 = Serializer.toString(mmap2);
		assertNotNull(text2);
		assertEquals("{key1=0,key2=\"value\"}", text2);
	}
	
	@Test
	public void testRecognizeType() {
		assertEquals(true, Serializer.recognizeType("true", false));
		assertEquals(false, Serializer.recognizeType("false", false));
		assertEquals("value", Serializer.recognizeType("value", true));
		assertEquals(12, ((Number) Serializer.recognizeType("12", false)).intValue());
		assertEquals(12345678900l, ((Number) Serializer.recognizeType("12345678900", false)).longValue());
		assertEquals(12.3f, ((Number) Serializer.recognizeType("12.3", false)).floatValue(), 0.01);
	}
	
	@Test
	public void testConvert() {
		assertEquals("Иван", Serializer.convert("&#1048;&#1074;&#1072;&#1085;"));
	}
}
